const crypto = require('crypto');
const fs = require('fs');
const path = require('path');
const stream = require('stream');
const url = require('url');

const bottleneck = require('bottleneck');
const mime = require('mime-db');
const fetch = require('node-fetch');
const {promisify} = require('util');
const pipelinePromise = promisify(stream.pipeline)

class downloader {
	chf = 'sha256';//cryptographic hash function
	acceptableStatusCodes = [200];
	requestOpts = {
		timeout: 4000,
		compress: true,
		headers: {}
	};

	limiters = new bottleneck.Group({
	    maxConcurrent: 1,
	    minTime: 100
	});

	constructor() {}
	request(args) {
		return this.limiters.key(url.parse(args.url).hostname).schedule(() => this._request(args));
	}
	_request(args) {
		return fetch(args.url, this.requestOpts).then(async (res) => {
			if (this.acceptableStatusCodes.includes(res.status)) {
				// if hash doesn't exist, use first one we find
				if (!crypto.getHashes().includes(this.chf)) this.chf = crypto.getHashes()[0];
				const hash = crypto.createHash(this.chf);

				const pipeArgs = [res.body];
				let filename = '';
				if (args.filename) {
					const trans = new stream.Transform();
					trans._transform = function(chunk, encoding, callback) {
						hash.update(chunk);
						return callback(null, chunk);
					}

					//if the file is being saved, work out the extension/filename
					let ext = '';
					if (args.assumeExt === true) {
						if (res.headers.get('content-type')) {
							ext = `.${mime[res.headers.get('content-type')].extensions[0]}`;
						} else {
							const pathname = url.parse(args.url).pathname;
							ext = path.parse(pathname).ext.trim();
						}
					}
					filename = `${args.filename}${ext}`;

					pipeArgs.push(trans);
					pipeArgs.push(this.pipeToFile(filename));
				} else {
					pipeArgs.push(hash);
				}

				const pp = await pipelinePromise(pipeArgs);
				const digest = hash.digest('hex');
				const outObj = {
					url: args.url,
					statusCode: res.status,
					hashDigest: digest,
					hashAlgorithm: this.chf
				};
				if (args.filename) outObj.filename = filename;
				return outObj;
			} else {
			 	return {
					url: args.url,
					statusCode: res.status,
					hashDigest: res.status,
					hashAlgorithm: 'statusCode'
				};
			}
			
		});
	}
	

	addHeader(key, val) {
		this.requestOpts.headers[key] = val;
	}

	
	pipeToFile(fp, opts) {
		const parsedPath = path.parse(fp).dir;
		if (parsedPath != '') fs.mkdirSync(parsedPath, {recursive:true});
		return fs.createWriteStream(fp, opts);
	}
}


module.exports = new downloader();