# Introduction

This module is a wrapper around http/https requests to handle downloads and provides a checksum of the content.
The default checksum is SHA256.

## usage

### request(options)

Options object

* **url** - Fully qualified uri string
* **filename** - Basename of the file once it's downloaded. If this option is not included then the file will not be saved to disk.
* **assumeExt** - Assume the extension of the file being saved.


```
const dl = require('downloader');
console.log(await dl.request({url:'http://127.0.0.1/large.png', filename: 'foo', assumeExt: true}));

/*
{
  url: 'http://127.0.0.1/large.png',
  statusCode: 200,
  hashDigest: '97b2dbbf1e1cbff4d7caab2c981bf272491e3b7b4c816eab594efd0fb8d69678',
  hashAlgorithm: 'sha256'
}
*/
```